def gridChallenge(grid):
    for i in range(len(grid)):
        grid[i] = list(grid[i])
        grid[i].sort()
    length = len(grid[0])
    for i in range(length):
        for j in range(len(grid) - 1):
            if ord(grid[j][i]) > ord(grid[j + 1][i]):
                return "NO"
    return "YES"
