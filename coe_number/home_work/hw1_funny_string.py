def funny_string(s):
    rever_s = s[::-1]
    for i in range(len(s) - 1):
        if abs(ord(s[i]) - ord(s[i + 1])) != abs(ord(rever_s[i]) - ord(rever_s[i + 1])):
            return "Not Funny"
    return "Funny"
