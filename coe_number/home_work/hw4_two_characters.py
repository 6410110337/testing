from itertools import combinations


def alternate(s):
    max_len = 0
    new_s = combinations(set(s), 2)
    for i in new_s:
        char = None
        count = 0
        for j in s:
            if j in i:
                if char == None:
                    char = j
                    count += 1
                    continue
                if j == char:
                    count = 0
                    break
                else:
                    char = j
                    count += 1
        if count > max_len:
            max_len = count
    return max_len
