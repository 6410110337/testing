import string


def caesarCipher(s, k):
    k %= 26
    anw = ""
    char = string.ascii_lowercase
    for i in s:
        if i.isalpha():
            index = ord(i) + k
            if ord(i) <= 90:
                if index > 90:
                    index = (index % 90) + 64
                anw += chr(index)
            else:
                if index > 122:
                    index = (index % 122) + 96
                anw += chr(index)
        else:
            anw += i
    return anw
