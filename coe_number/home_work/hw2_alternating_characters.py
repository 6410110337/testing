def alternating_characters(s):
    anw = 0
    for i in range(len(s) - 1):
        if s[i] == s[i + 1]:
            anw += 1
    return anw
