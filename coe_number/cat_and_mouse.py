def cat_mouse(x: int, y: int, z: int) -> str:
    if max(x, y, z) > 100 or min(x, y, z) < 1:
        return "Out of range"
    distan_cat_a_mouse = abs(x - z)
    distan_cat_b_mouse = abs(y - z)
    if distan_cat_a_mouse < distan_cat_b_mouse:
        return "Cat A"
    if distan_cat_b_mouse < distan_cat_a_mouse:
        return "Cat B"
    return "Mouse C"
