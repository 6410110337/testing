def stair(n: int, display: str) -> str:
    if n < 1 or n > 30:
        return "Out of range"
    anw = ""
    for i in range(1, n):
        anw += f"{' '*(n - i)}{display * i}\n"
    anw += f"{display * n}"
    return anw
