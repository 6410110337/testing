from coe_number.home_work.hw1_funny_string import funny_string

import unittest


class HomeWork1Test(unittest.TestCase):
    def test_give_abcd_is_Funny(self):
        text = "abcd"
        excepted_output = "Funny"

        text = funny_string(text)

        self.assertEqual(text, excepted_output)

    def test_give_bossy_is_Not_Funny(self):
        text = "bossy"
        excepted_output = "Not Funny"

        text = funny_string(text)

        self.assertEqual(text, excepted_output)

    def test_give_acgkm_is_Funny(self):
        text = "acgkm"
        excepted_output = "Funny"

        text = funny_string(text)

        self.assertEqual(text, excepted_output)

    def test_give_difxif_is_Not_Funny(self):
        text = "difxif"
        excepted_output = "Not Funny"

        text = funny_string(text)

        self.assertEqual(text, excepted_output)

    def test_give_zsvnfib_is_Funny(self):
        text = "zsvnfib"
        excepted_output = "Funny"

        text = funny_string(text)

        self.assertEqual(text, excepted_output)

    def test_give_HLOTVJDURNXOIY_is_Not_Funny(self):
        text = "HLOTVJDURNXOIY"
        excepted_output = "Not Funny"

        text = funny_string(text)

        self.assertEqual(text, excepted_output)

    def test_give_AIYVCPDUIVCFVN_is_Funny(self):
        text = "AIYVCPDUIVCFVN"
        excepted_output = "Funny"

        text = funny_string(text)

        self.assertEqual(text, excepted_output)
