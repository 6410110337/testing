from coe_number.home_work.hw5_grid_challenge import gridChallenge

import unittest


class HomeWork1Test(unittest.TestCase):
    def test_give_abcd_efgh_ijkl_mnop_is_YES(self):
        text = ["abcd", "efgh", "ijkl", "mnop"]
        excepted_output = "YES"

        text = gridChallenge(text)

        self.assertEqual(text, excepted_output)

    def test_give_bossy_issoi_facke_homes_youes_is_NO(self):
        text = ["bossy", "issoi", "facke", "homes", "youes"]
        excepted_output = "NO"

        text = gridChallenge(text)

        self.assertEqual(text, excepted_output)

    def test_give_acgkka_adiylb_adlymd_belzne_cepzog_dfpzzi_is_YES(self):
        text = ["acgkka", "adiylb", "adlymd", "belzne", "cepzog", "dfpzzi"]
        excepted_output = "YES"

        text = gridChallenge(text)

        self.assertEqual(text, excepted_output)

    def test_give_dif_xif_ied_bed_aha_is_NO(self):
        text = ["dif", "xif", "ied", "bed", "aha"]
        excepted_output = "NO"

        text = gridChallenge(text)

        self.assertEqual(text, excepted_output)

    def test_give_EGACD_CCGFH_DCIHF_IEFIC_KDFJJ_EJIML_TOQPU_is_YES(self):
        text = ["EGACD", "CCGFH", "DCIHF", "IEFIC", "KDFJJ", "EJIML", "TOQPU"]
        excepted_output = "YES"

        text = gridChallenge(text)

        self.assertEqual(text, excepted_output)

    def test_give_HLOT_IASY_OKMX_PEES_FIFA_GGEZ_is_NO(self):
        text = ["HLOT", "IASY", "OKMX", "PEES", "FIFA", "GGEZ"]
        excepted_output = "NO"

        text = gridChallenge(text)

        self.assertEqual(text, excepted_output)

    def test_give_eabcd_fghij_olkmn_trpqs_xywuv_is_YES(self):
        text = ["eabcd", "fghij", "olkmn", "trpqs", "xywuv"]
        excepted_output = "YES"

        text = gridChallenge(text)

        self.assertEqual(text, excepted_output)
