from coe_number.home_work.hw3_caesar_cipher import caesarCipher

import unittest


class HomeWork3Test(unittest.TestCase):
    def test_give_abcd_and_1_is_bcde(self):
        text = "abcd"
        shif = 1
        excepted_output = "bcde"

        text = caesarCipher(text, shif)

        self.assertEqual(text, excepted_output)

    def test_give_bossy_and_33_is_ivzzf(self):
        text = "bossy"
        shif = 33
        excepted_output = "ivzzf"

        text = caesarCipher(text, shif)

        self.assertEqual(text, excepted_output)

    def test_give_I_am_good_student_and_7_is_P_ht_nvvk_zabklua(self):
        text = "I am good student"
        shif = 7
        excepted_output = "P ht nvvk zabklua"

        text = caesarCipher(text, shif)

        self.assertEqual(text, excepted_output)

    def test_give_shap_dot_dot_underscorce_obffl_and_13_is_shap_dot_dot_underscorce_bossy(
        self,
    ):
        text = "#.._obffl"
        shif = 13
        excepted_output = "#.._bossy"

        text = caesarCipher(text, shif)

        self.assertEqual(text, excepted_output)

    def test_give_ggEZ_5555_exclamation_and_88_is_qqOJ_5555_exclamation(self):
        text = "ggEZ 5555!!!"
        shif = 88
        excepted_output = "qqOJ 5555!!!"

        text = caesarCipher(text, shif)

        self.assertEqual(text, excepted_output)

    def test_give_xox_or_sos_or_zoz_and_9_is_gxg_xa_bxb_xa_ixi(self):
        text = "xox or sos or zoz"
        shif = 9
        excepted_output = "gxg xa bxb xa ixi"

        text = caesarCipher(text, shif)

        self.assertEqual(text, excepted_output)

    def test_give_dot_dot_dot_Xmffq_dot_dot_dot_and_8_is_dot_dot_dot_Funny_dot_dot_dot(
        self,
    ):
        text = "...Xmffq..."
        shif = 8
        excepted_output = "...Funny..."

        text = caesarCipher(text, shif)

        self.assertEqual(text, excepted_output)
