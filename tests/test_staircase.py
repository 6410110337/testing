from coe_number.staircase import stair

import unittest


class TestStaircase(unittest.TestCase):
    def test_give_3_and_sharp(self):
        n = 3
        display = "#"
        expected_output = "  #\n ##\n###"

        output = stair(n, display)

        self.assertEqual(output, expected_output)

    def test_give_0_and_sharp_is_out_of_range(self):
        n = 0
        display = "#"
        expected_output = "Out of range"

        output = stair(n, display)

        self.assertEqual(output, expected_output)

    def test_give_5_and_star(self):
        n = 5
        display = "*"
        expected_output = "    *\n   **\n  ***\n ****\n*****"

        output = stair(n, display)

        self.assertEqual(output, expected_output)

    def test_give_10_and_o(self):
        n = 10
        display = "o"
        expected_output = (
            "         o\n"
            + "        oo\n"
            + "       ooo\n"
            + "      oooo\n"
            + "     ooooo\n"
            + "    oooooo\n"
            + "   ooooooo\n"
            + "  oooooooo\n"
            + " ooooooooo\n"
            + "oooooooooo"
        )

        output = stair(n, display)

        self.assertEqual(output, expected_output)

    def test_give_25_and_plus(self):
        n = 25
        display = "+"
        expected_output = (
            "                        +\n"
            + "                       ++\n"
            + "                      +++\n"
            + "                     ++++\n"
            + "                    +++++\n"
            + "                   ++++++\n"
            + "                  +++++++\n"
            + "                 ++++++++\n"
            + "                +++++++++\n"
            + "               ++++++++++\n"
            + "              +++++++++++\n"
            + "             ++++++++++++\n"
            + "            +++++++++++++\n"
            + "           ++++++++++++++\n"
            + "          +++++++++++++++\n"
            + "         ++++++++++++++++\n"
            + "        +++++++++++++++++\n"
            + "       ++++++++++++++++++\n"
            + "      +++++++++++++++++++\n"
            + "     ++++++++++++++++++++\n"
            + "    +++++++++++++++++++++\n"
            + "   ++++++++++++++++++++++\n"
            + "  +++++++++++++++++++++++\n"
            + " ++++++++++++++++++++++++\n"
            + "+++++++++++++++++++++++++"
        )

        output = stair(n, display)

        self.assertEqual(output, expected_output)

    def test_give_7_and_double_x(self):
        n = 7
        display = "xx"
        expected_output = (
            "      xx\n"
            + "     xxxx\n"
            + "    xxxxxx\n"
            + "   xxxxxxxx\n"
            + "  xxxxxxxxxx\n"
            + " xxxxxxxxxxxx\n"
            + "xxxxxxxxxxxxxx"
        )

        output = stair(n, display)

        self.assertEqual(output, expected_output)

    def test_give_35_and_star(self):
        n = 35
        display = "*"
        expected_output = "Out of range"

        output = stair(n, display)

        self.assertEqual(output, expected_output)
