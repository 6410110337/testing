from coe_number.home_work.hw2_alternating_characters import alternating_characters

import unittest


class HomeWork2Test(unittest.TestCase):
    def test_give_aaaabbabaccca_is_6(self):
        text = "aaaabbabaccca"
        excepted_output = 6

        result = alternating_characters(text)

        self.assertEqual(result, excepted_output)

    def test_give_bossy_is_1(self):
        text = "bossy"
        excepted_output = 1

        result = alternating_characters(text)

        self.assertEqual(result, excepted_output)

    def test_give_AACCDAAFERCSEAASFWSSTTT_is_7(self):
        text = "AACCDAAFERCSEAASFWSSTTT"
        excepted_output = 7

        result = alternating_characters(text)

        self.assertEqual(result, excepted_output)

    def test_give_hiii_my_name_boss_is_3(self):
        text = "hiii my name boss"
        excepted_output = 3

        result = alternating_characters(text)

        self.assertEqual(result, excepted_output)

    def test_give_AAAAAAAAAAAASSSSASASSSANNNSUUASKXCJDJDJDDDJVOWWW_is_23(self):
        text = "AAAAAAAAAAAASSSSASASSSANNNSUUASKXCJDJDJDDDJVOWWW"
        excepted_output = 23

        result = alternating_characters(text)

        self.assertEqual(result, excepted_output)

    def test_give_AabBcldrSAFLWlsmopAskdfcSDW_is_0(self):
        text = "AabBcldrSAFLWlsmopAskdfcSDW"
        excepted_output = 0

        result = alternating_characters(text)

        self.assertEqual(result, excepted_output)

    def test_give_ajoedookdfjwncswofrmcxswks_is_1(self):
        text = "ajoedookdfjwncswofrmcxswks"
        excepted_output = 1

        result = alternating_characters(text)

        self.assertEqual(result, excepted_output)
