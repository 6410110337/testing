from coe_number.home_work.hw4_two_characters import alternate

import unittest


class HomeWork3Test(unittest.TestCase):
    def test_give_aaaabbabaccca_is_0(self):
        text = "aaaabbabaccca"
        excepted_output = 0

        result = alternate(text)

        self.assertEqual(result, excepted_output)

    def test_give_bossy_is_2(self):
        text = "bossy"
        excepted_output = 2

        result = alternate(text)

        self.assertEqual(result, excepted_output)

    def test_give_aerzdsfazazryugmgtahfzahfgzazhafjfgyrzaz_is_16(self):
        text = "aerzdsfazazryugmgtahfzahfgzazhafjfgyrzaz"
        excepted_output = 16

        result = alternate(text)

        self.assertEqual(result, excepted_output)

    def test_give_arieerlwwkkqkqbwhrkxitjvhka_is_4(self):
        text = "arieerlwwkkqkqbwhrkxitjvhka"
        excepted_output = 4

        result = alternate(text)

        self.assertEqual(result, excepted_output)

    def test_give_AHOEsPCaadasMDadHUsiekdmAdfOHSsfdUAfdkosiurliecoHAKSIWH_is_4(self):
        text = "AHOEsPCaadasMDadHUsiekdmAdfOHSsfdUAfdkosiurliecoHAKSIWH"
        excepted_output = 4

        result = alternate(text)

        self.assertEqual(result, excepted_output)

    def test_give_AabBcldrSAFLWlsmopAskdfcSDW_is_5(self):
        text = "AabBcldrSAFLWlsmopAskdfcSDW"
        excepted_output = 5

        result = alternate(text)

        self.assertEqual(result, excepted_output)

    def test_give_ahgirkemcahahahitembbvkooqalohahanolikehsdkitaheeeahqweirolcvah_is_20(
        self,
    ):
        text = "ahgirkemcahahahitembbvkooqalohahanolikehsdkitaheeeahqweirolcvah"
        excepted_output = 20

        result = alternate(text)

        self.assertEqual(result, excepted_output)
