from coe_number.cat_and_mouse import cat_mouse

import unittest


class CatAndMouseTest(unittest.TestCase):
    def test_give_1_2_3_is_cat_b(self):
        x, y, z = 1, 2, 3
        expected_output = "Cat B"

        output = cat_mouse(x, y, z)

        self.assertEqual(output, expected_output)

    def test_give_100_1_49_is_cat_b(self):
        x, y, z = 100, 1, 49
        expected_output = "Cat B"

        output = cat_mouse(x, y, z)

        self.assertEqual(output, expected_output)

    def test_give_63_29_49_is_cat_a(self):
        x, y, z = 63, 29, 49
        expected_output = "Cat A"

        output = cat_mouse(x, y, z)

        self.assertEqual(output, expected_output)

    def test_give_5_2_10_is_cat_a(self):
        x, y, z = 5, 2, 10
        expected_output = "Cat A"

        output = cat_mouse(x, y, z)

        self.assertEqual(output, expected_output)

    def test_give_99_1_50_is_mouse_c(self):
        x, y, z = 99, 1, 50
        expected_output = "Mouse C"

        output = cat_mouse(x, y, z)

        self.assertEqual(output, expected_output)

    def test_give_24_90_57_is_mouse_c(self):
        x, y, z = 24, 90, 57
        expected_output = "Mouse C"

        output = cat_mouse(x, y, z)

        self.assertEqual(output, expected_output)

    def test_give_n4_101_8_is_out_of_range(self):
        x, y, z = -4, 101, 8
        expected_output = "Out of range"

        output = cat_mouse(x, y, z)

        self.assertEqual(output, expected_output)
