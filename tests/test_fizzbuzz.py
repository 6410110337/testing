from coe_number.fizzbuzz import fizz_buzz

import unittest


class FizzBuzzTest(unittest.TestCase):
    def test_give_9_is_fizz(self):
        x = 9

        x = fizz_buzz(x)

        self.assertIs(x, "Fizz")

    def test_give_15_is_fizzbuzz(self):
        x = 15

        x = fizz_buzz(x)

        self.assertIs(x, "FizzBuzz")

    def test_give_250_is_buzz(self):
        x = 250

        x = fizz_buzz(x)

        self.assertIs(x, "Buzz")

    def test_give_133_is_not_fizzbuzz(self):
        x = 133

        x = fizz_buzz(x)

        self.assertIsNone(x)

    def test_give_123_is_fizz(self):
        x = 123

        x = fizz_buzz(x)

        self.assertIs(x, "Fizz")

    def test_give_12000_is_fizzbuzz(self):
        x = 12000

        x = fizz_buzz(x)

        self.assertIs(x, "FizzBuzz")

    def test_give_4705_is_buzz(self):
        x = 4705

        x = fizz_buzz(x)

        self.assertIs(x, "Buzz")
