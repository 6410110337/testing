from coe_number.number_utils import is_prime_list

import unittest


class PrimeListTest(unittest.TestCase):
    def test_give_1_2_3_is_not_prime(self):
        prime_list = [1, 2, 3]

        is_prime = is_prime_list(prime_list)

        self.assertFalse(is_prime)

    def test_give_7_3_5_is_prime(self):
        prime_list = [7, 3, 5]

        is_prime = is_prime_list(prime_list)

        self.assertTrue(is_prime)

    def test_give_2_4_8_is_not_prime(self):
        prime_list = [2, 4, 8]

        is_prime = is_prime_list(prime_list)

        self.assertFalse(is_prime)

    def test_give_599_311_127_1009_is_prime(self):
        prime_list = [599, 311, 127, 1009]

        is_prime = is_prime_list(prime_list)

        self.assertTrue(is_prime)

    def test_give_13_20_5_70_89_is_not_prime(self):
        prime_list = [13, 20, 5, 70, 89]

        is_prime = is_prime_list(prime_list)

        self.assertFalse(is_prime)

    def test_give_191_277_131_29_11_41_is_prime(self):
        prime_list = [191, 277, 131, 29, 11, 41]

        is_prime = is_prime_list(prime_list)

        self.assertTrue(is_prime)

    def test_give_n13_2_3_5_7_is_not_prime(self):
        prime_list = [-13, 2, 3, 5, 7]

        is_prime = is_prime_list(prime_list)

        self.assertFalse(is_prime)
